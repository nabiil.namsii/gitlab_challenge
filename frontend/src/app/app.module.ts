import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import { MatTableModule } from '@angular/material/table'

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
// eslint-disable-next-line max-len
import {GitlabProjectsComponent} from './gitlab-projects/gitlab-projects.component';
import {HttpClientModule} from '@angular/common/http';
import { FormsModule } from '@angular/forms';

// eslint-disable-next-line new-cap
@NgModule({
  declarations: [
    AppComponent,
    GitlabProjectsComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatTableModule,
    FormsModule],
  providers: [],
  bootstrap: [AppComponent]})
// eslint-disable-next-line require-jsdoc
export class AppModule {}
