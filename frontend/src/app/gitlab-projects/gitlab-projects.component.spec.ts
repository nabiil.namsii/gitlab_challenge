import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GitlabProjectsComponent } from './gitlab-projects.component';

describe('GitlabProjectsComponent', () => {
  let component: GitlabProjectsComponent;
  let fixture: ComponentFixture<GitlabProjectsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GitlabProjectsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GitlabProjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
