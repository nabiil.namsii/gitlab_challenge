/* eslint-disable linebreak-style */
/* eslint-disable padded-blocks */
/* eslint-disable space-before-blocks */
/* eslint-disable linebreak-style */
/* eslint-disable require-jsdoc */
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

// eslint-disable-next-line new-cap
@Injectable({providedIn: 'root'})
export class ProjectService{
  private url = 'http://localhost:3011/api/v1/';
  constructor(private httpClient: HttpClient) { }
  getProjects(){
    return this.httpClient.get(this.url);
  }
}
