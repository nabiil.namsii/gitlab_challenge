/* eslint-disable max-len */
/* eslint-disable indent */
/* eslint-disable arrow-parens */
/* eslint-disable new-cap */
/* eslint-disable require-jsdoc */
import { Component, OnInit } from '@angular/core'
import { ProjectService } from './services/projects'
export interface PElement {
    name: string
    starCount: number
}
@Component({
    selector: 'app-gitlab-projects',
    templateUrl: './gitlab-projects.component.html',
    styleUrls: ['./gitlab-projects.component.css'],
})
export class GitlabProjectsComponent implements OnInit {
    displayedColumns: string[] = ['name', 'stars'];
    projects: PElement[] = [];
    searchValue: any;
    data:any;
    constructor(private service: ProjectService) {}
    filterByValue(array: any, string: any) {
        return array.filter((p: any) => {
            return p.name.toLowerCase().indexOf(string?.toLowerCase()) !== -1;
        });
    }
    onKey(event: any) {
        this.searchValue = event.target.value;
        this.projects = this.filterByValue(this.data, this.searchValue);
    }
    ngOnInit() {
        this.service.getProjects().subscribe((response: any) => {
            const DATA: PElement[] = response;
            this.projects = DATA;
            this.data=DATA;
        });
    }
}
