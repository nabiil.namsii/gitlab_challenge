import { Request, Response } from 'express';
import { request, gql } from 'graphql-request'

const QUERY_LAST_PROJECTS = gql`
    query last_projects($n: Int) {
        projects(last: $n) {
            nodes {
                name
                starCount
                description
            }
        }
    }
`

const getProjects = async (req: Request, res: Response, next: any) => {
    try {
        request('https://gitlab.com/api/graphql', QUERY_LAST_PROJECTS, {
            n: 5,
        }).then((data) => res.json(data.projects.nodes))
    } catch (err: any) {
        console.error(`Error while getting programming languages`, err.message)
        next(err)
    }
}

const getStarsSum = async (req: Request, res: Response, next: any) => {
    try {
        const { n } = req.body;
        request('https://gitlab.com/api/graphql', QUERY_LAST_PROJECTS, {
            n
        }).then((data) =>{
          let totalCount = 0;
          for (const node of data.projects.nodes) {
            totalCount += node.starCount
          }
          res.json({totalCount})
        })
    } catch (err: any) {
        console.error(`Error while getting programming languages`, err.message)
        next(err)
    }
}

export default {
    getProjects,
    getStarsSum,
}
