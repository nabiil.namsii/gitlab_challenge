import express from 'express'
import { router } from './routes';
const app = express()
app.use(express.json())
app.use((req, res, next) =>  {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
   next();
  });
app.use('/api/v1', router);


app.listen(3011, () => {
    console.log(`Server Started at ${3011} 🚀🚀`)
})
