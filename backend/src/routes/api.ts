import express from 'express'
import { getLastProjects } from '../controllers';
const router = express.Router();

router.get('/', getLastProjects.getProjects );
router.post('/', getLastProjects.getStarsSum );


export default router;